var TableModel = function (w, h)
{
    /*this.value = v || "";*/
    if (typeof (w) == 'number' && w >= 0)
	      this.width = Math.floor(w);
    else
	      this.width = 1;

    if (typeof (h) == 'number' && h >= 0)
	      this.height = Math.floor(h);
    else
	      this.height = 1;

    this.cells = [];
    for (i = 0; i < this.height; i++)
    {
	      this.cells[i] = [];
	      for (j = 0; j < this.width; j++)
	      {
	          this.cells[i][j] = new Cell();
	      }
    }
};

TableModel.prototype = (function () {

    var lastColumnRec = function (w)
    {
	      return (w >= 26 ? lastColumnRec ((w / 26 >> 0) - 1) : '') + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' [w % 26 >> 0];
    };

    var getCellAux = function (c, r, that)
    {
	      var c_int = 0;
	      var r_int = parseInt (r);
	      c = c.toUpperCase ();

	      // premiere ite a -65 (A = 0)
	      c_int += (c[i].charCodeAt(0) - "A".charCodeAt(0)) * Math.pow (26, i);
	      // les autres a -64
	      for (i = 1; i < c.length; i++)
	      {
	          c_int += (c[i].charCodeAt(0) - "A".charCodeAt(0) -1) * Math.pow (26, i);
	      }
	      var res = that.cells[r_int][c_int];

	      return res;
    };

    var insertLineAtIdx_aux = function (that, i)
    {
	      var line = [];
        var ite = 0;
	      for (ite = 0; ite < that.width; ite++) {line[ite] = new Cell ();}
	      that.cells.splice (i, 0, line);
    };

    var insertColumnAtIdx_aux = function (that, j)
    {
        var ite = 0;
        var cellC;
        for (ite = 0 ; ite < that.height ; ite++)
        {
            cellC = new Cell ();
            that.cells[ite].splice (j, 0, cellC);
        }
    };

    var insertColumnBefore_aux = function (that, c)
    {
        var c_int = 0;
        var l = c.length;

        // traitement de c
        for (var i = 0 ; i<l ; i++) {

            var curr = c[l-i-1];
            console.log (curr);

            //test de validite de la valeur de c
            if (curr.charCodeAt(0) < 65 || curr.charCodeAt(0) > 91) {
                console.log ("Invalid Argument", c[l-i-1], c_int); return;
            }

            // on recupere la valeur en base 10 du numero de colonne c
            // ascii de c[i] * 26^i : ex : AC = A*21^1 + C*26^0
            c_int += (curr.charCodeAt(0)-64) * (Math.pow (26,  i));
            console.log (c_int, c[l-i-1]);
        }

        // c_int est initialise, c ne doit plus etre utilise

        var ite = 0;
        var cellC;
        for (ite = 0 ; ite < that.height ; ite++)
        {
            cellC = new Cell ();
            // ex : C -> nb 3 donc apres insertion -> C nb 4 et new -> 3 || A B C -> A B new C
            that.cells[ite].splice (c_int-1, 0, cellC);
        }
    };

    var insertColumnAfter_aux = function (that, c) {
        var c_int = 0;
        var l = c.length;

        // traitement de c
        for (var i = 0 ; i<l ; i++) {

            var curr = c[l-i-1];
            console.log (curr);

            //test de validite de la valeur de c
            if (curr.charCodeAt(0) < 65 || curr.charCodeAt(0) > 91) {
                console.log ("Invalid Argument", c[l-i-1], c_int); return;
            }

            // on recupere la valeur en base 10 du numero de colonne c
            // ascii de c[i] * 26^i : ex : AC = A*21^1 + C*26^0
            c_int += (curr.charCodeAt(0)-64) * (Math.pow (26,  i));
        }

        // c_int est initialise, c ne doit plus etre utilise

        var ite = 0;
        var cellC;
        for (ite = 0 ; ite < that.height ; ite++)
        {
            cellC = new Cell ();
            // ex : C -> nb 3 donc apres insertion -> C nb 4 et new -> 3 || A B C -> A B new C
            that.cells[ite].splice (c_int+1, 0, cellC);
        }
    };

    return {
	      getWidth: function () {return this.width;},
	      getHeight: function () {return this.height;},
	      firstLine: function () {
	          if (this != undefined) return "1";
	          else return "";
	      },
	      firstColumn: function () {
	          if (this != undefined) return "A";
	          else return "";
	      },
	      lastLine: function () {
	          if (this != undefined) return ""+this.width;
	          else return "";
	      },
	      lastColumn : function () {return lastColumnRec(this.height);},
	      getCell: function (c, r) {
	          return getCellAux (c, r, this);
	      },
	      getCellAtIdx: function (i, j) {
	          if (this == undefined) {console.log("ERROR, argument 'this' is undefined");}

	          return this.cells[i][j];
	      },
	      insertLineAtIdx: function (i) {
	          insertLineAtIdx_aux (this, i);
            this.height += 1;
	      },
	      // le tableur est pour les faibles donc debut a 1 pas -2 car +1 par le decalage du a l'insertion
	      insertLineBefore: function (r) {
	          /*
	           function InvalidArgException(m) {
		         this.name = "InvalidArgException";
		         this.message = m;
	           }*/

	          if (parseInt (r).isNaN || parseInt(r)<1)
	          {console.log ("ERROR");}
	          else
	          {
		            this.insertLineAtIdx (parseInt(r) - 1);
                this.height += 1;
	          }
	      },
	      insertLineAfter: function (r) {

	          if (parseInt (r).isNaN || parseInt(r)<1)
	          {console.log ("ERROR");}
	          else
	          {
		            this.insertLineAtIdx (parseInt(r));
                this.height += 1;
	          }
	      },
        insertColumnAtIdx: function (j) {

            console.log ("dans insert", j);
            insertColumnAtIdx_aux(this, j);
            this.width += 1;
        },
        insertColumnBefore : function (c) {

            insertColumnBefore_aux (this, c);
            this.width += 1;
        },
        insertColumnAfter : function (c) {

            insertColumnAfter_aux (this, c);
            this.width += 1;
        }
    };
})();


// ------------------------------------------- TEST ---------------------------------------------- //
/* isNaN(x)*/
var t = new TableModel (4, 3);

for (i = 0; i < 3; i++) {
    for (j = 0; j < 4; j++) {
	      t.cells[i][j] = new Cell("a");
    }
}

//var y = new TableModel ("gdfhjkjgjhlm");
//debugW = t.getWidth ();
//console.log (t, y, t.cells);
//console.log (debugW, t.width);
//console.log (t);
//console.log (t.lastColumn());
//t.getCell ('C', '4');
//console.log (t);
//t.insertLineAtIdx (1);
//console.log (t, "here2");
//t.insertLineAfter ('3');
console.log (t, "hereEND");
//t.insertColumnAtIdx (2);
t.insertColumnBefore ("C");
console.log (t, "hereColumn");
t.insertColumnAfter (t, "after");
console.log (t);

